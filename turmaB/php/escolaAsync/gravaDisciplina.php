<?php
require("conecta.php");
$x=file_get_contents('php://input');
$x=json_decode($x);

$codigo=$x->txtCodigo;
$nome=$x->txtNome;
$ch=$x->txtCH;


$sql="insert into escola.disciplina(codigo,nome,cargahoraria)
      values(:par_codigo,:par_nome,:par_ch)";
$stmt = $conn->prepare($sql);
$dados=array(":par_codigo"=>$codigo,
             ":par_nome"=>$nome,
             ":par_ch"=>$ch             
            );
$result=$stmt->execute($dados);

if($result){
    $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
}
else{
    $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));

