document.addEventListener("DOMContentLoaded",()=>{
    let cont=document.querySelector("#lblcontador");
    let botaoInc=document.querySelector("#btnInc");
    let botaoDec=document.querySelector("#btnDec");
    botaoInc.addEventListener("click",()=>{
        cont.innerHTML++;
    });
    botaoDec.addEventListener("click",()=>{
        cont.innerHTML--;
        if(cont.innerHTML<0){
            cont.innerHTML=0;
        }
    });
  
});

    