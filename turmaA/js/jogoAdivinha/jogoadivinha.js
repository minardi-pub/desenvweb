document.addEventListener("DOMContentLoaded",()=>{

    function sorteia(){
        let max=10;
        let s=Math.floor(Math.random()*max)+1;
        return s;
    }
    let n=sorteia();
    let botaoTentar=document.querySelector("#btnTentar");
    let tentativas=document.querySelector("#lbltentativas");
    let botaoReiniciar=document.querySelector("#btnReiniciar");
    botaoTentar.addEventListener("click",()=>{
        let palpite=document.querySelector("#num").value;
        let res=""
        if(palpite==n){
            res="Você acertou!";
        }
        else if(palpite>n){
            res="Chutou alto!";
            tentativas.innerHTML--;
            if(tentativas.innerHTML<=0){
                botaoTentar.disabled=true;
            }
        }
        else{
            res="Chutou baixo!";
            tentativas.innerHTML--;
            if(tentativas.innerHTML<=0){
                botaoTentar.disabled=true;
            }
        }
        document.querySelector("#resultado").innerHTML=res;
        
    });
    botaoReiniciar.addEventListener("click",()=>{
        n=sorteia();
        document.querySelector("#resultado").innerHTML="";
        botaoTentar.disabled=false;
        tentativas.innerHTML=3;

    })

});