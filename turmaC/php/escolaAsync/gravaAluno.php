<?php
ini_set("display_errors",1);
require("conecta.php");
/*OBS: para os content-types:
{headers: {'content-type': 'multipart/form-data'}} e 
{headers: {'content-type': 'application/x-www-form-urlencoded'}}
deve ser usado $_POST
caso contrário use file_get_contents('php://input')
*/

$x=file_get_contents('php://input');

$x=json_decode($x);

$ra=$x->txtRA;
$nome=$x->txtNome;
$endereco=$x->txtEnd;
$curso=$x->selCurso;
$estado=$x->selEstado;
$cidade=$x->selCidade;
$cep=$x->txtCep;

$sql="insert into escola.aluno(ra,nome,endereco,
      curso,estado,cidade,cep)
      values(:par_ra,:par_nome,:par_end,
             :par_curso,:par_est,:par_cid,:par_cep)";
$stmt = $conn->prepare($sql);
$dados=array(":par_ra"=>$ra,
             ":par_nome"=>$nome,
             ":par_end"=>$endereco,
             ":par_curso"=>$curso,
             ":par_est"=>$estado,
             ":par_cid"=>$cidade,
             ":par_cep"=>$cep
            );
$result=$stmt->execute($dados);

if($result){
    $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
}
else{
    $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));