<?php
session_start();
require("conecta.php");
$dadosForm=file_get_contents("php://input");
$dadosForm=json_decode($dadosForm);
$ra=$dadosForm->txtRA;
$senha=$dadosForm->txtPW;

$sql="select ra,nome 
      from escola.aluno 
      where ra=:par_ra and senha=sha2(:par_senha,256)";
$stmt=$conn->prepare($sql);
$stmt->execute(array(":par_ra"=>$ra,":par_senha"=>$senha));
$result=$stmt->fetchAll(PDO::FETCH_OBJ);
if(count($result)>0){
    //usuario/senha ok
    $_SESSION["logado"]=true;
    $_SESSION["usuario"]=$result[0]->nome;
    $msg=array("codigo"=>1,"texto"=>"Seja bem-vindo.");
}
else{
    //usuario/senha incorretos
    $msg=array("codigo"=>0,"texto"=>"Usuário/Senha incorretos.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));
      