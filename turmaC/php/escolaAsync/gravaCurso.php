<?php
require("conecta.php");
$x=file_get_contents('php://input');
$x=json_decode($x);

$codigo=$x->txtCodigo;
$nome=$x->txtNome;


$sql="insert into escola.curso(codigo,nome)
      values(:par_codigo,:par_nome)";
$stmt = $conn->prepare($sql);
$dados=array(":par_codigo"=>$codigo,
             ":par_nome"=>$nome,
            );
$result=$stmt->execute($dados);

if($result){
    $msg=array("codigo"=>1,"texto"=>"Registro inserido com sucesso.");
}
else{
    $msg=array("codigo"=>0,"texto"=>"Erro ao inserir.");
}
header('Content-Type: application/json; charset=utf-8');
echo(json_encode($msg));

